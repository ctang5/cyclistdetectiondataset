# Overview

This Roboflow dataset composes of many synthetic and edge-case angles for cyclist detection. This dataset was created by Charles Tang, a high school student at the Massachusetts Academy of Math and Science located at WPI. This dataset will be used in the project *A Blind Spot Alert Apparatus for Cyclists in Right-Turning Semi-trailer Trucks*, which will be used to provide further object detection training in addition to the CIMAT cyclist dataset for the blind spot cyclist detection apparatus. 

**Abstract**
In order to reduce the number of truck-cyclist accidents, this study designs a visual-based blind spot warning system for a semi-trailer truck. A large number of cyclist collisions are caused by semi-trailer trucks, more specifically, in right-hook turns. First, the object detection model was created using state-of-the-art lightweight deep learning architectures trained on a cyclist image dataset, which is used to locate and detect cyclists actively. Next, the object detection model was deployed onto a Google Coral Dev Board mini-computer with a camera module and analyzed for accuracy and speed. Lastly, the combined blind spot detection device will be tested in real-time to model traffic scenarios and analyzed further for performance, feasibility, and further work of the apparatus. 

*Keywords*: Bicyclist safety, blind spots, vulnerable road users, object detection, semi-trailer trucks, right-hook turns, cyclist collisions, warning system

**Sources of Images**
* Parallel Domain synthetic bicycle detection dataset
* Other Roboflow cyclist sources
* Online digital sources

Please contact me at ctang5@wpi.edu for any questions or concerns.
